## Rules

The object of the game is to collect the most matching pairs.
choose a card, then select another card and turns it over.
If the cards are not a match they are turned back over.
Winning the game once all the cards have been played.

## Install

`cd memory`

`yarn install`

`yarn start`
